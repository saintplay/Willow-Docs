=================
Análisis y Diseño
=================

Documentos relacionados con el análisis y diseño del software.

.. note::
    Se emplearán términos y conceptos que son propios del ecosistema de este ámbito comercial, por favor asegúrese de revisar primero
    :ref:`La lista de definiciones <definiciones_willow>`
