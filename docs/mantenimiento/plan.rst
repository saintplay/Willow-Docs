=====================
Plan de Mantenimiento
=====================

El plan de mantenimiento define los intervalos de tiempo en los que se harán desarrollo, mantenimiento LTS y mantenimiento.

* .. image:: /_static/img/release_plan.png
     :alt: Willow: Calendario de Mantenimiento

Calendario Detallado
--------------------

.. table:: Calendario de Mantenimiento
    :align: center
    :widths: grid

    +----------------+-------------+------------+----------------------+---------------+
    | Versión        | Alias       | LTS Inicio | Mantenimiento Inicio | Mantenimiento |
    +================+=============+============+======================+===============+
    | **Web 1.x**    | Pluton      | 17-05-2018 | 03-10-2019           | 18-02-2021    |
    +----------------+-------------+------------+----------------------+---------------+
    | **Web 2.x**    | Express     | 01-11-2018 | 19-03-2020           | 05-08-2021    |
    +----------------+-------------+------------+----------------------+---------------+
    | **Web 3.x**    | Conqueror   | 18-04-2019 | 18-02-2021           | 20-01-2022    |
    +----------------+-------------+------------+----------------------+---------------+
    | **Mobile 1.x** | Maya        | 03-10-2019 | 18-02-2021           | 20-01-2022    |
    +----------------+-------------+------------+----------------------+---------------+
    | **Web 4.x**    |                          *No Definido*                          |
    +----------------+-------------+------------+----------------------+---------------+
    | **Mobile 2.x** |                          *No Definido*                          |
    +----------------+-------------+------------+----------------------+---------------+

Fases de Mantenimiento
----------------------

.. note::
    *Beta*, significa que el sistema todavía no se encuentra completamente estable, es un periodo indispensable antes de pasar a LTS.

LTS
^^^

LTS o Mantenimiento LTS es el periodo de tiempo en el que se da prioridad a un sistema cuando se presentan errores o vulnerabilidades.

- Durante este periodo el mantenimiento debería ser gratuito.
- Durante LTS no se agregan funcionalidades ni cambios mayores.

Mantenimiento
^^^^^^^^^^^^^

El Mantenimiento es el periodo de tiempo en el que se soporte básico a un sistema cuando se presentan errores o vulnerabilidades.

- Durante este periodo el mantenimiento debería ser pagado.


.. seealso::

  :ref:`Composición de Ramas en Git <composicion_ramas>`
    Explicación detallada de la función de cada rama usada en el control de versiones.

  :ref:`Roadmap <roadmap>`
    Explicación detallada de las funcionalidades del sistema agrupadas por versiones.

  :ref:`Versionamiento <versionamiento>`
    Explicación de las reglas que se toman en cuenta para versionar el sistema.
