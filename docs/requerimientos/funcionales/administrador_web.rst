====================================
Requerimientos Web del Administrador
====================================

Requerimientos Web del Administrador

RF-1:  ENTRAR AL SISTEMA MEDIANTE UN REGISTRO
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** Para poder entrar a una cuenta administrativa, el sistema protegerá el acceso mediante un usuario y una contraseña.
- **Prioridad:** 5

RF-2:  LLEVAR CONTROL DE LOS CONTRATOS
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** El sistema tendrá la información de los contratos, mostrándolos visualmente en un planograma.
- **Prioridad:** 5

RF-3:  CONTROLAR EL LA INFORMACIÓN ESPACIAL DE LOS STANDS
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** El sistema debe permitir controlar la ubicación y tamaño de los stands, de esta manera se podrá visualizar correctamente su locación en el planograma.
- **Prioridad:** 5

RF-4:  REGISTRAR NUEVO PAGO DEL CONTRATO
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** El sistema deberá permitir la adición de pagos de mensualidad por contrato.
- **Prioridad:** 5

RF-5:  MODIFICAR LOS CONTRATOS
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** El sistema deberá permitir la modificación de contratos.
- **Prioridad:** 4

RF-6:  ADMINISTRAR LOS FONDOS
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** El sistema deberá permitir la visualización, adición, modificación y eliminación de los fondos monetarios donde fluye el capital de la empresa.
- **Prioridad:** 4

RF-7:  ADMINISTRAR LOS INGRESOS
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** El sistema deberá permitir la visualización, adición, modificación y eliminación de los ingresos que registra la empresa.
- **Prioridad:** 4

RF-8:  ADMINISTRAR LOS GASTOS 
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** El sistema deberá permitir la visualización, adición, modificación y eliminación de los gastos que registra la empresa.
- **Prioridad:** 4

RF-9:  GENERAR REPORTE DE MENSUALIDADES
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** Con el reporte se podrá ver todas las mensualidades del contrato desde su inicio, viendo los montos, las fechas y los datos del pago.
- **Prioridad:** 3

RF-10:  REGISTRAR RENOVACIÓN DE CONTRATO
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** El sistema deberá permitir la adición de renovaciones de contrato, los cuales significan que a partir de la fecha ingresada, se cambiará la mensualidad al monto indicado también.
- **Prioridad:** 3

RF-11:  VISUALIZAR LISTA DE DEUDORES
---------------------------------------------

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

- **Descripción:** El sistema deberá permitir ver una lista de deudores, mostrando sus datos y los montos que deben.
- **Prioridad:** 3
