.. _uso_de_trello:

=============
Uso de Trello
=============

Uso de Trello.

`Link <https://trello.com/b/QgnFOsus/willow>`_

Guía de Etiquetas
-----------------

1. Estado del Item
^^^^^^^^^^^^^^^^^^

- **Item Defered:** Indica que el item  perteneció inicialmente a un sprint anterior
- **Doing:** Indica que el elemento se esta realizando
- **Done:** Indica que el elemento ha sido implementado

2. Respectos a su implicancia
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- **Needs Code Review:** Indica que el código implementado necesita ser supervisado
- **Code Reviewed:** El código implementado ha sido revisado y aprobado al menos por otro desarrollador
- **Needs Testing:** Indica que la funcionalidad necesita pruebas automatizadas
- **Needs UX Discussion:** Indica que la usabilidad de la funcionalidad necesita ser discutida


3. Respecto al impacto del Cambio
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- **Breaking Change:** Indica que el cambio no será compatible con anteriores versiones
- **Major Change:** Indica que el cambio significa una mejora mayor para la versión actual
- **Minor Change:** Indica que el cambio es mínimo y no debería 

4. Respecto al tipo de tarea
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- **Design:** Indica que la tarea está relacionada con diseño, ilustración o prototipado
- **Engineering:** Indica que la tarea requiere investigación y de ingeniar una solución para un problema que no se puede resolver actualmente
- **Docs:** Indica que la tarea es acerca de documentación
