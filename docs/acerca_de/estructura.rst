Estructura de la Documentación
------------------------------

La presente está divida en 6 secciones *(sin contar esta misma)* debido a que estos 6 últimos comúnmente son considerados como ciclos del
desarrollo del software.

.. note::
    Es importante recalcar que el repositorio(*.git*) que contiene la presente documentación sigue siendo un controlador de versiones, y por lo tanto se deben respetar las reglas esenciales de estos, como:

    - No guardar imágenes que no sean completamente necesarias.
    - Los cambios se podrán hacer solo por personas autentificadas.
    - etc.
