Tabla de Origen Para Los Artefactos
-----------------------------------

En la presente documentación se encontrará una tabla al inicio de cada artefacto, la cual será llamada **"tabla de origen"**,
se usará como alternativa a las *nomenclaturas* y darán información adicional.

======================  =============================
EMPRESA                 LOCALES                      
======================  =============================
*Nombre de la empresa*  *Nombre del o de los locales*
======================  =============================

- **Empresa:** El nombre de le empresa a la cual pertenece el artefacto.
- **Locales:** El nombre del local o de los locales al cual pertenece el artefacto.

Ejemplos
^^^^^^^^

Esta notación denota que el artefacto aplica para todas las empresas y (*por consiguiente*) para todos los locales

=======  =======
EMPRESA  LOCALES
=======  =======
TODOS    TODOS  
=======  =======

Esta notación denota que el artefacto aplica para el local *GAMARRA* de la empresa *CARRUSEL*

========  =======
EMPRESA   LOCALES
========  =======
CARRUSEL  GAMARRA
========  =======

Esta notación denota que el artefacto aplica para todos los locales de la empresa *NIBA*

=======  =======
EMPRESA  LOCALES
=======  =======
NIBA     TODOS  
=======  =======

Esta notación denota que el artefacto aplica para el local *MEGAPLAZA* de la empresa *NIBA* y así mismo para los locales *GAMARRA* y *SAN JUAN* de la empresa *CARRUSEL*

+----------+------------+
|  EMPRESA |   LOCALES  | 
+==========+============+
|   NIBA   |  MEGAPLAZA |
+----------+------------+
| CARRUSEL | - GAMARRA  |
|          | - SAN JUAN |
+----------+------------+
