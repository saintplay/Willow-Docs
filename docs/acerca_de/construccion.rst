.. _construccion_documentacion:

================================
Construcción de la Documentación
================================

.. note::
    No confundir con :ref:`Construcción del Software(Desarrollo) <desarrollo>`

- **Trello** para almacenar las tareas de cada Sprint y mostrar el avance y requisitos de todos los sistemas, más información en :ref:`Uso de Trello <uso_de_trello>`
- **Gitlab** es un hosting para control de versiones *Git*, llama la atención debido a su refinado diseño y su comunicación directa con sus usuarios. Además de poseer muchas herramientas que otros servicios no cuentan. Más información en :ref:`Uso de Gitlab <uso_de_gitlab>`
- **Sphinx** es un motor open source escrito en *Python* para enlazar documentos y generar documentación, para escribir en Sphinx se deberá usar **Markdown** o **reEstructuredText** Más información en :ref:`Uso de Sphinx <uso_de_sphinx>`
- **Read The Docs** es un hosting para documentación en formato Sphinx, es completamente gratuito y open source. Más información en :ref:`Uso de Read The Docs <uso_de_read_the_docs>`
- **Google Drive** para almacenar los binarios que son muy pesados o archivos que no tiene sentido guardarlos en el control de versiones, como por ejemplo archivos fuente de Adobe Illustrator, Fotos de Contratos, etc. Más información en :ref:`Uso de Google Drive <uso_de_google_drive>`
- **Draw.io** es un servicio cloud open source gratuito que nos permite hacer diagramas y tablas, este software será nuestra herramienta principal para crear los documentos de análisis y diseño, etc.
- **VSCode:** opcional, pero altamente recomendable para editar el código fuente de la documentación. El repositorio viene con una configuración para este editor de código que ayudará a los colaboradores. Más información en :ref:`Uso de Visual Studio Code <uso_de_vscode>`

**Contenido**

.. toctree::
   :glob:

   construccion/trello
   construccion/gitlab
   construccion/sphinx
   construccion/read_the_docs
   construccion/google_drive
   construccion/vscode
