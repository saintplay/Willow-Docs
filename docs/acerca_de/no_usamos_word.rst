¿Por qué no usamos Microsoft Word?
----------------------------------
Creemos que el stack de tecnologías escogido es sencillo y poderoso, desde nuestra perspectiva de desarrolladores es suficiente para la documentación que se necesita en este proyecto.
Nos da muchas comodidades y flexibilidad, que como desarrolladores, nos gusta.

Además de seguir las buenas prácticas y sugerencias establecidas por la comunidad Open Source.

Ventajas respecto a Microsoft Word
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- No se tiene que lidiar con problemas de versiones de Word(2009, 2010, 2013, 2016, etc)
- No se necesita contar con Word para editar el documento, esto facilita la lectura y edición para usuarios de **Mac OSX** y de **Linux**
- Es amigable para el desarrollador, esto quiere decir que resulta más conveniente y más rápido escribir en Markdown o en reEstructuredText
- Se integra perfectamente con un controlador de versiones como Git, a diferencia de Word, Markdown permite sacarle todo el provecho a la herramienta de diferenciación(*diff*) de Git
- Este stack det tecnologías es open source y completamente gratuito, no nos debemos preocupar de licencias
- Read The Docs nos permite convertir la documentación a **HTML**, **PDF** o incluso como **e-Book** de manera instantánea
- Los artefactos(*textos*) son increíblemente ligeros, reducen el espacio ocupado por la documentación
- Read The Docs genera una vista online que está bien diseñada y es responsiva para computadoras de escritorio y móviles
- Read The Docs nos permite intercambiar de versiones de manera muy simple
- Read The Docs nos ofrece una webhook para poder hacer desarrollo continuo, es decir, hacemos cambios al repositorio de la documentación y se verán reflejados automáticamente
- Sphinx nos da un cuadro de búsqueda
- Sphinx soporte traducciones para la documentación

Desventajas respecto a Microsoft Word
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- El código fuente de los artefactos no es tan fácil de entender para los que no conocen de programación de etiquetas
- No hay mucho control de  colores, sombreados y herramientas de edición avanzadas
- No se pueden incrustar imágenes por documento, solo se pueden enlazar desde otras locaciones
- Hacer Tablas puede llegar a ser un poco tedioso
- No tiene soporte para detección de errores de ortografía, aunque varios editores de código y texto pueden solucionar el tema
- Si no se conoce Markdown y reEstructuredText, se requiere de un esfuerzo(*mínimo*) para poder escribir la documentación, a diferencia de Word, donde todo el mundo sabe usarlo
