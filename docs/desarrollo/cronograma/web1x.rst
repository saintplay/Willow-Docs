================
Web 1.x (Pluton)
================

.. note::
  Esta versión actualmente está en desarrollo, por lo que el cronograma se verá a través del tablero de proyecto en :ref:`Trello <uso_de_trello>`.

  Debido a esto, cada *sprint backlog* será documentado **después del periodo activo de desarrollo**

La primera versión estable de Willow deberá implementar un producto mínimo viable para la administración de stands, contará con funcionalidades escenciales
como el flujo de caja, visualización de planograma, administración de trabajadores, clientes, etc.

**Contenido**

.. toctree::
   :glob:

   web1x/sprints
