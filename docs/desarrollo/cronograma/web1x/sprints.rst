===============
Web 1.x Sprints
===============

.. note::
  Esta versión actualmente está en desarrollo, por lo que el cronograma se verá a través del tablero de proyecto en :ref:`Trello <uso_de_trello>`.

  Debido a esto, cada *sprint backlog* será documentado **después del periodo activo de desarrollo**
