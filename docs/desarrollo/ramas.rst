.. _composicion_ramas:

================
Sistema de Ramas
================

En está sección se detallará el uso de ramas(Git) para un adecuado control de versiones.


------
Master
------

La rama master

--------------------------
Ramas de Cambios Complejos
--------------------------

Ejemplos:

- *feature-print-outlays*
- *fix-excel-table-generation*

--------------------------
Ramas de Versiones Mayores
--------------------------

Ejemplos:

- *feature-print-outlays*
- *fix-excel-table-generation*

----------------------
Ramas de Cliente Final
----------------------

- *niba*
- *carrusel*

.. note::
    **¿Tienes ganas de indagar más en el tema?** Te recomendamos leer este `artículo <http://nvie.com/posts/a-successful-git-branching-model/>`_ sobre **una adecuada estructura para organizar ramas de Git**

