===============================
Guía para empezar a desarrollar
===============================

.. note::
    Este sistema es capaz de ejecutarse en cualquier sistema operativo: **Windows**, **MacOS** o **Linux**

------------------
Software Requerido
------------------

- `Git (> 2) <https://git-scm.com/>`_
- `Node.js (> 8) <https://nodejs.org/es/>`_
- `Yarn (> 1.3) <https://yarnpkg.com/es/>`_
- `Firebase tools (> 3) <https://github.com/firebase/firebase-tools>`_


Además requerirás del acceso al **repositorio del código fuente** y una cuenta de `Firebase <https://console.firebase.google.com/>`_


--------------------
Proyecto de Firebase
--------------------

**1. Crear o usar un Proyecto**

El acceso a los proyectos reales está limitado, en cambio se recomienda que 
cada desarrollador se cree o comparta un proyecto.

Para ello se deberá revisar la `documentación de Firebase <https://firebase.google.com/docs/web/setup>`_

**2. Activar la autenticación**

Willow(*hasta la fecha*) funciona con autenticación en base a **correo y contraseña**, este módulo deberá ser activado
y posteriormente se deberán crear algunos usuarios de prueba


--------------------
Construir el entorno
--------------------

**1. Clonar el proyecto**

Link del repositorio: `https://gitlab.com/saintplay/willow <https://gitlab.com/saintplay/willow>`_

**2. Instalar el repositorio**

Ejecutar el comando `yarn install`

--------------------
Variables de entorno
--------------------

.. note::
    Si se desconoce del tema, por favor revisar `Variables de entorno <variables_entorno>`_

Se deberá crear un archivo *.env* en la raíz del proyecto, luego siguiendo la estructura de *.env.example*
(raíz del proyecto) se deberán sustituir las variables por las adecuadas

--------
Procesos
--------

**Para iniciar el sistema en la red local**

.. highlight:: c
yarn dev

**Para formatear el código**

.. highlight:: c
yarn lint

**Para testear el código**

.. highlight:: c
yarn test

**Para construir el sistema**

.. highlight:: c
yarn build

**Para desplegar el sistema**

.. highlight:: c
yarn deploy

* Más información en el archivo package.json*
