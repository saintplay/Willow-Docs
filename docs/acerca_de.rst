.. _acerca_de:

===================================
Acerca de la Presente Documentación
===================================

Esta sección explicará a detalle el **¿Cómo?**, **¿Qué?** y **¿Por qué?** de la presente documentación.

**Contenido**

.. toctree::
   :glob:

   acerca_de/definiciones
   acerca_de/estructura
   acerca_de/construccion
   acerca_de/no_usamos_word
   acerca_de/tabla_origen
   acerca_de/colaboradores

Objetivo del Presente
---------------------

¿Cual es la motivación detrás de la presente documentación, que se quiere lograr o transmitir hacia los lectores?

Estado Actual de la Documentación
---------------------------------

.. note::
    Como parte de la primera entrega del curso **Evolución de Software**,
    esta presentación esta enfocada en mostrar innovación en como se documenta un software, ya que **representa un esfuerzo por emplear herramientas
    y metodologías que son ampliamente conocidas en las comunidades Open Source**, pero en las Universidades ni se les hace mención.

    Esta presentación contiene diagramas de análisis y diseño, **pero no su totalidad**

==============================  ==========  =========  ==================
Sección                         Estructura  Contenido  Gráficos/Diagramas
==============================  ==========  =========  ==================
**Requerimientos**              100%        100%       *No es necesario* 
**Análisis y Diseño**           100%        60%        60%               
**Construcción del Software**   100%        20%        0%                
**Pruebas del Software**        100%        0%         0%                
**Integración del Software**    100%        60%        0%                
**Mantenimiento del Software**  100%        30%        30%               
==============================  ==========  =========  ==================
