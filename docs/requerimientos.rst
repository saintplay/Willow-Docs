==============
Requerimientos
==============

Requerimientos del sistema.

.. note::
    Se emplearán términos y conceptos que son propios del ecosistema de este ámbito comercial, por favor asegúrese de revisar primero
    :ref:`La lista de definiciones <definiciones_willow>`


Perspectiva del Producto
------------------------

Perspectiva del Producto


Funciones del Producto
----------------------

Funciones del Producto


Características del Usuario
---------------------------

Características del Usuario

Restricciones
-------------

Restricciones


Suposiciones y Dependencias
---------------------------

Suposiciones y Dependencias


Estructura de los Requerimientos
--------------------------------

Estructura de los Requerimientos


Lista de Requerimientos
-----------------------

.. toctree::
   :glob:

   requerimientos/funcionales
   requerimientos/no_funcionales
   requerimientos/restricciones
   requerimientos/interfaces
